package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.PortUnreachableException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.Integer.parseInt;

/**
 * Aceasta clasa citeste din fisier si pregateste comenzile cerute
 */
public class InputPreparator {
    private ClientBLL clientBLL;
    private OrderBLL orderBLL;
    private ProductBLL productBLL;
    private OutputGenerator outputGenerator;

    private int contorIdClient, contorIdOrder, contorIdProduct;

    public InputPreparator(String arg) throws Exception {
        clientBLL = new ClientBLL();
        orderBLL = new OrderBLL();
        orderBLL.deleteAll();
        productBLL = new ProductBLL();
        outputGenerator =  new OutputGenerator();

        contorIdOrder = 0;
        contorIdClient = 0;
        contorIdProduct = 0;
        reeadFromFile(arg);
    }

    public void reeadFromFile(String fileName) throws Exception {
        try {
            String processedLine;
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            while((processedLine = reader.readLine()) != null) {
                String name;
                if(processedLine.contains(": ")) {
                    if(processedLine.contains(", ")) {
                        name = processedLine.substring(processedLine.indexOf(":") + 1, processedLine.indexOf(","));
                    } else {
                        name = processedLine.substring(processedLine.indexOf(":") + 1);
                    }
                    processedLine = processedLine.replace(name, "");
                    name = name.trim().replaceAll("\\s+", " ");
                } else {
                    name = null;
                }
                processedLine = processedLine.replaceAll(",", "");
                processedLine = processedLine.replaceAll(":", "");
                processedLine = processedLine.trim().replaceAll("\\s+", " ");

                List<String> inputData;
                inputData = Arrays.asList(processedLine.split(" "));
                parseCommands(inputData, name);

            }
        }catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda care prelucreaza inputData, si anume comenzile date in fisierul text.
     * De asemenea apeleaza functiile pentru a pregati rezultatele
     * Insereaza clienti si produse noi
     * Printeaza report-uri despre clienti, produse si comenzi
     * Sterge clienti sau produse
     * Face comenzi noi
     * @param inputData
     * @param name
     * @throws Exception
     */
    public void parseCommands(List<String> inputData, String name) throws Exception {
        if(inputData.get(0).equalsIgnoreCase("Insert")) {
            if(inputData.get(1).equalsIgnoreCase("client")) {
                clientBLL.insertNewClient(++contorIdClient, name, inputData.get(2));
            } else if(inputData.get(1).equalsIgnoreCase("product")) {
                productBLL.insertNewProduct(++contorIdProduct, name, parseInt(inputData.get(2)), Integer.parseInt(inputData.get(3)));
            }
        }
        if(inputData.get(0).equalsIgnoreCase("Report")) {
            if(inputData.get(1).equalsIgnoreCase("client")) {
                outputGenerator.printAll((ArrayList<?>) new ClientDAO().findAll());
            } else if(inputData.get(1).equalsIgnoreCase("product")) {
                outputGenerator.printAll((ArrayList<?>) new ProductDAO().findAll());
            } else {
                outputGenerator.printAll((ArrayList<?>) new OrderDAO().findAll());
            }
        }
        if(inputData.get(0).equalsIgnoreCase("Delete")) {
            if(inputData.get(1).equalsIgnoreCase("client")) {
                clientBLL.deleteClient(name);
            } else if(inputData.get(1).equalsIgnoreCase("product")) {
                productBLL.deleteProduct(name);
            }
        }
        if(inputData.get(0).equalsIgnoreCase("Order")) {
            int id = ++contorIdOrder;
            id++;
            orderBLL.insertNewOrder(id, name, inputData.get(1), Integer.parseInt(inputData.get(2)));
        }
    }
}
