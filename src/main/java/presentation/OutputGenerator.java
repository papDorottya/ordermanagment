package presentation;

import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.OrderSumDAO;
import dao.ProductDAO;
import model.Client;
import model.Orderr;
import model.OrderSum;
import model.Product;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Metoda pentru a indenta textul in tabele, luand datele din baza de date si pentru a genera pdf-ul cu aceasta
 */
public class OutputGenerator {
    public void printAll(ArrayList<?> listToPrint) {
        Document document = new Document();
        try {
            if (!listToPrint.isEmpty()) {
                PdfWriter.getInstance(document, new FileOutputStream("aaa/" + listToPrint.get(0).getClass().getName().substring(listToPrint.get(0).getClass().getName().indexOf(".") + 1) + new Timestamp(System.currentTimeMillis()).toString().replaceAll(":", "") + ".pdf"));
                document.open();
                document.add(new Paragraph((listToPrint.get(0).getClass().getName().substring(listToPrint.get(0).getClass().getName().indexOf(".") + 1) + "\n\n")));
                PdfPTable table = new PdfPTable(listToPrint.get(0).getClass().getDeclaredFields().length - 1);
                for (Field field : listToPrint.get(0).getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    if (!field.getName().equals("id")) {
                        PdfPCell firstRow = new PdfPCell(new Paragraph(field.getName().toUpperCase()));
                        firstRow.setBackgroundColor(BaseColor.LIGHT_GRAY);
                        firstRow.setBorderWidth(4);
                        table.addCell(firstRow);
                    }
                }

                for (Object iterator : listToPrint) {
                    for (Field field : iterator.getClass().getDeclaredFields()) {
                        field.setAccessible(true);
                        if (!field.getName().equals("id")) {
                            table.addCell(field.get(iterator).toString());
                        }
                    }
                }
                document.add(table);
            }
        } catch (FileNotFoundException | DocumentException | IllegalAccessException exception) {
            exception.printStackTrace();
        }

        document.close();
    }

    /**
     * Metoda pentru a informa clientul daca nu avem suficiente produse in stoc
     *
     * @param name
     * @param product
     * @param quantity
     */
    public void PrintNotEnoughOnStoc(String name, String product, int quantity) {
        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream("ProductList" + name.replaceAll(" ", "") + ".pdf"));
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.BLACK);
        Paragraph paragraph = new Paragraph("INFO ABOUT PRODUCTS! \nFor the product: " + product.toUpperCase() + " with the quantity of: " + quantity + " we don't have enough, we have only " + new ProductDAO().findByName(product).getQuantity() + " pieces.", font);
        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();
    }

    /**
     * Metoda pentru a crea "factura" totala a unui client
     * Genereaza un pdf cu comana unui client
     *
     * @param client
     */
    public void printOrder(Client client) {
        Document document = new Document();
        OrderDAO orderDAO = new OrderDAO();
        ProductDAO productDAO = new ProductDAO();
        try {
            PdfWriter.getInstance(document, new FileOutputStream("Receipt" + client.getName().replaceAll(" ", "") + ".pdf"));
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.BLACK);
        Paragraph paragraph = new Paragraph("RECEIPT\n Name:" + client.getName().toUpperCase(), font);
        paragraph.add("\n\n" + "Purchased list: ");
        List<Orderr> orders = orderDAO.findAllByClientId(client.getId());
        for (Orderr iterator : orders) {
            Product productBought = productDAO.findByID(iterator.getProductId());
            paragraph.add("\n" + "Product: " + productBought.getName().toUpperCase() + "; quantity: " + iterator.getQuantity());
        }
        OrderSumDAO orderSumDAO = new OrderSumDAO();
        OrderSum orderSum = orderSumDAO.findByClientId(client.getId());
        paragraph.add("\n" + "The total price is: " + orderSum.getTotalPrice());
        try {
            document.add(paragraph);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();

    }

    /**
     * Metoda care afiseaza toate toate comenzile efectuate pana acum
     * Si datele despre acestea indentate sub forma de tabel
     *
     * @param orders
     */
    public void printAllOrders(ArrayList<Orderr> orders) {
        Document document = new Document();
        try {
            if (!orders.isEmpty()) {
                PdfWriter.getInstance(document, new FileOutputStream("Orders" + new Timestamp(System.currentTimeMillis()).toString().replaceAll(":", "") + ".pdf"));
                document.open();
                PdfPTable table = new PdfPTable(3);
                PdfPCell header = new PdfPCell(new Paragraph("NAME"));
                header.setHorizontalAlignment(Element.ALIGN_CENTER);
                header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                header.setBorderWidth(2);
                table.addCell(header);
                PdfPCell header2 = new PdfPCell(new Paragraph("PRODUCT"));
                header.setHorizontalAlignment(Element.ALIGN_CENTER);
                header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                header2.setBorderWidth(2);
                table.addCell(header2);
                PdfPCell header3 = new PdfPCell(new Paragraph("QUANTITY"));
                header.setHorizontalAlignment(Element.ALIGN_CENTER);
                header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                header3.setBorderWidth(2);
                table.addCell(header3);
                PdfPCell header4 = new PdfPCell(new Paragraph("TOTAL PRICE TO PAY"));
                header.setHorizontalAlignment(Element.ALIGN_CENTER);
                header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                header4.setBorderWidth(2);
                table.addCell(header4);

                for (Orderr iterator : orders) {
                    Client client = new ClientDAO().findByID(iterator.getClientId());
                    Product product = new ProductDAO().findByID(iterator.getProductId());
                    OrderSum orderSum = new OrderSumDAO().findByClientId(iterator.getClientId());
                    table.addCell(client.getName());
                    table.addCell(product.getName());
                    table.addCell(String.valueOf(iterator.getQuantity()));
                    table.addCell(String.valueOf(orderSum.getTotalPrice()));
                }
                document.add(table);
            }
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
        document.close();
    }

}
