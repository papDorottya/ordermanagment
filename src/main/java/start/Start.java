package start;

import presentation.InputPreparator;

public class Start{
    /**
     * Metoda principala care porneste aplicatia
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        if(args.length == 1){
            System.out.println(args[0]);
            new InputPreparator(args[0]);
        } else{
            System.out.println("Input only the input file");
        }

    }
}
