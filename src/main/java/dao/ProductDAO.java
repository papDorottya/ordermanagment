package dao;

import connection.ConnectionFactory;
import model.Client;
import model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

public class ProductDAO extends AbstractDAO<Product> {
    /**
     * Creaza o conexiune cu baza de date
     * Cauta produsul cu numele dat
     *
     * @param name
     * @return produsul din baza de date, cu numele corespunzator valorii parametrului "name"
     */
    public Product findByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("name");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            List<Product> productList = createObjects(resultSet);
            if (productList.isEmpty()) {
                return null;
            } else {
                return productList.get(0);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Creaza o conexiune cu baza de date
     * Sterge produsul cu numele dat
     *
     * @param name
     * @return numarul de coloane afectate in urma operatiei
     */
    public Integer deleteByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteQuery("name");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            return statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:deleteByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

}
