package dao;

import connection.ConnectionFactory;

import javax.xml.transform.Result;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clasa responsabila pentru a prelucra datele din MySQL, inlocuieste query-urile din MySQL
 *
 * @param <T> - reprezinta clasa, respectiv tabelul asupra care vom face diferite operatii dorite
 * @author Pap Dorottya
 */
public class AbstractDAO<T> {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
    /**
     * tipul
     */
    public final Class<T> type;

    /**
     * Instantierea unui nou Abstract DAO
     */
    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Creez query-ul SELECT din SQL
     *
     * @return query-ul SELECT intr-un string
     */
    private String createSelectQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT");
        sb.append(" * ");
        sb.append("FROM ");
        sb.append(type.getSimpleName());

        return sb.toString();
    }

    /**
     * Creez query-ul SELECT din SQL utilizand parametrii campurilor
     *
     * @param field the field
     * @return query-ul SELECT intr-un string
     */
    public String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT");
        sb.append(" * ");
        sb.append("FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");

        return sb.toString();
    }

    /**
     * Creeaza o conexiune cu baza de date
     * Executa query-ul si apeleaza o functie pt a crea obiectul mai apoi returnat
     *
     * @return ArrayList a clasei T care corespunde cu datele din tabela T
     */
    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement("Select * FROM " + type.getSimpleName().toLowerCase());
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Creeaza o conexiune cu baza de date
     * Executa query-ul si apeleaza o functie pt a crea obiectul mai apoi returnat
     *
     * @param id - pe care il cautam in baza de date
     * @return o instanta a clasei T care corespunde cu datele din tabela T
     */
    public T findByID(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById" + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Creez query-ul INSERT din SQL utilizand parametrii campurilor
     *
     * @return query-ul INSERT intr-un string
     */
    private String createInsertQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(type.getSimpleName());
        sb.append(" values ( ");
        for (Field field : type.getDeclaredFields()) {
            sb.append("? , ");
        }
        sb.deleteCharAt(sb.length() - 2);
        sb.append(")");

        return sb.toString();
    }

    /**
     * Insereaza un obiect a clasei T in baza de date
     *
     * @param t
     * @return id-ul Obiectului inserat
     */
    public Integer insert(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createInsertQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int contor = 0;
            for (Field field : t.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                Object value = field.get(t);
                statement.setObject(++contor, value);
            }
            System.out.println(statement.toString());
            return statement.executeUpdate();
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + " DAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Creez query-ul UPDATE din SQL utilizand parametrii campurilor
     *
     * @param value
     * @param field
     * @return query-ul UPDATE intr-un string
     */
    public String createUpdateQuery(String value, String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET " + value + " =?");
        sb.append(" WHERE " + field + " =?");

        return sb.toString();
    }

    /**
     * Updateaza un obiect din baza de date
     *
     * @param value         valoarea
     * @param updatingField campul care va urma sa fie actualizat
     * @param id            id-ul
     * @return o val. booleana, true daca a reusit, false daca nu
     */
    public boolean update(Object value, String updatingField, int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createUpdateQuery(updatingField, "id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setObject(1, value);
            statement.setObject(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
            return false;
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return true;
    }

    /**
     * Creez query-ul DELETE din SQL utilizand parametrii campurilor
     *
     * @param field
     * @return query-ul DELETE intr-un string
     */
    public String createDeleteQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + "=?");

        return sb.toString();
    }

    /**
     * Creez query-ul DELETE din SQL utilizand parametrii campurilor
     *
     * @return query-ul DELETE intr-un string
     */
    public String createDeleteALLQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(type.getSimpleName());

        return sb.toString();
    }

    /**
     * Sterge un obiect din baza de date
     *
     * @param id id-ul
     * @return o val. booleana, true daca a reusit, false daca nu
     */
    public boolean delete(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
            return false;
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return true;
    }

    /**
     * Creaza obiecte de un anumit tip dintr-un resultSet
     * Acesta reprezentand rezultatului unei query care a fost deja executat
     *
     * @param resultSet
     * @return o lista de obiecte de tipul clasei T
     */
    public List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();
        try {
            while (resultSet.next()) {
                T instance = type.newInstance();

                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor((String) field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException | IllegalAccessException | SecurityException | IllegalArgumentException | InvocationTargetException | SQLException | IntrospectionException e) {
            e.printStackTrace();
        }

        return list;
    }

}
