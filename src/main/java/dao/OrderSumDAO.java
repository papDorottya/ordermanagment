package dao;

import connection.ConnectionFactory;
import model.OrderSum;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

/**
 * Aceasta clasa ne ajuta sa colectam datele specifice pentru a prelucra totalul comenzilor
 *
 */
public class OrderSumDAO extends AbstractDAO<OrderSum> {
    /**
     * Metoda pentru a accesa lista de obiecte care corespund id-ul clientului dat
     * @param id
     * @return o lista de order sum pentru a calcula suma care trebuie sa o plateasca clientul cu id-ul dat
     */
    public OrderSum findByClientId(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("clientId");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            List<OrderSum> orderSumList = createObjects(resultSet);
            if (orderSumList.isEmpty())
                return null;
            else
                return orderSumList.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findByClientId " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Sterge comanda totala a clientului cu id-ul dat
     *
     * @param id
     * @return numarul coloanelor afectate
     */
    public int deleteByClientId(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteQuery("personId");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "DAO:deleteByClientIdOrderSum" + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return 0;
    }

    /**
     * Updateaza totalul unui client, dat de id-ul acestuia,in baza de date
     * Cu valoarea data prin parametru, in campul dorit
     *
     * @param updatingField
     * @param id
     * @param value
     * @return numarul de coloane afectate
     */
    public int updateByClientId(String updatingField, int id, Object value) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createUpdateQuery(updatingField, "clientId");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setObject(1, value);
            statement.setObject(2, id);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "DAO:updateByClientIdOrderSum " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return 0;
    }
}
