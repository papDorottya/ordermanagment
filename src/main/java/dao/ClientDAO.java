package dao;

import connection.ConnectionFactory;
import model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Clasa responsabila pentru a prelucra datele din MySQL
 * Aceste metode fiind specifice doar clientilor
 * Celelealte metode specifice se mostenesc din clasa AbstractDAO
 *
 */
public class ClientDAO extends AbstractDAO<Client> {
    /**
     * Metoda care creaza o conexiune cu baza de date
     * Executa query-ul si apeleaza functia pt a crea obiectul care o sa fie returnat
     *
     * @param name
     * @return returneaza null daca nu s-a gasit clientul cu numele dat, sau obiectul creat in urma gasirii clientului
     */
    public Client findByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("name");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            List<Client> clientList = createObjects(resultSet);
            if(clientList.isEmpty())
                return null;
            else
                return clientList.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Creaza o conexiune cu baza de date
     * Si sterge clientul cu numele dat
     * @param name
     * @return numarul coloanelor afectate
     */
    public Integer deleteByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteQuery("name");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "DAO:deleteByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }
}
