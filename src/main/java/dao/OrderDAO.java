package dao;

import connection.ConnectionFactory;
import model.Client;
import model.Orderr;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Clasa care implementeaza metode specifice comenzilor asupra baza de date
 * !! Daca sunt rugata sa sterg un client din baza de date, si a comandat inainte, acesta nu va disparea de pe lista de comenzi
 * Se aplica ce este mai sus si la alimentele deja cumparate
 * Deci in concluzie am decis ca nu voi implementa nici o metoda
 */
public class OrderDAO extends AbstractDAO<Orderr> {
    /**
     * Metoda care cauta toate comenzile unui client pentru a face totalul
     * @param id
     * @return
     */
    public List<Orderr> findAllByClientId(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("clientId");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            List<Orderr> orderrList = createObjects(resultSet);
            if(orderrList.isEmpty())
                return null;
            else
                return orderrList;
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll from order " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    public boolean deleteAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteALLQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "DAO:deleteAll " + e.getMessage());
            return false;
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return true;
    }
}
