package model;

/**
 * Clasa care echivaleaza un tabel SQL
 * Reprezentand o comanda cu un identificator unic, id-ul clientului, al produsului, si cantitatea
 *
 * @autor Pap Dorottya
 */
public class Orderr {

    private int id;
    private int clientId;
    private int productId;
    private int quantity;

    /**
     * Instanteaza un obiect Order cu urmatorii parametrii
     *
     * @param id      the id
     * @param clientId    the client id
     * @param productId the product id
     * @param quantity the quantity
     */
    public Orderr(int id, int clientId, int productId, int quantity) {
        this.id = id;
        this.clientId = clientId;
        this.productId = productId;
        this.quantity = quantity;
    }

    /**
     * Constructor gol. Utilizat pentru crearea obiectului Order de la ...
     */
    public Orderr() {
    }

    /**
     * Metoda pentru a accesa campul ID din alte metode
     * @return Id-ul comenzii
     */
    public int getId() {
        return id;
    }

    /**
     * Metoda pentru a seta id-ul comenzii
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metoda pentru a accesa campul clientId din alte metode
     * @return Id-ul clientului
     */
    public int getClientId() {
        return clientId;
    }

    /**
     * Metoda pentru a seta id-ul clientului
     * @param clientId
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    /**
     * Metoda pentru a accesa campul ID din alte metode
     * @return Id-ul produsului
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Metoda pentru a seta id-ul produsului
     * @param productId
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * Metoda pentru a accesa campul cantitate din alte metode
     * @return cantitatea ramasa pe stoc
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Metoda pentru a seta cantitatea
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
