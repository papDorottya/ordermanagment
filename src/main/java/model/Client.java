package model;

/**
 * Clasa care echivaleaza un tabel SQL
 * Reprezentand un client cu un identificator unic, numele lui si adresa
 *
 * @autor Pap Dorottya
 */
public class Client {
    private int id;
    private String name;
    private String address;


    /**
     * Instanteaza un obiect Client cu urmatorii parametrii
     *
     * @param id      the id
     * @param name    the name
     * @param address the address
     */
    public Client(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    /**
     * Constructor gol. Utilizat pentru crearea obiectului Client de la ...
     */
    public Client() {}

    /**
     * Metoda pentru a accesa campul ID din alte metode
     * @return Id-ul clientului
     */
    public int getId() {
        return id;
    }

    /**
     * Metoda pentru a seta id-ul
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metoda pentru a accesa campul name din alte metode
     * @return Un string cu numele clientului
     */
    public String getName() {
        return name;
    }

    /**
     * Metoda pentru a seta name-ul
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Metoda pentru a accesa campul address din alte metode
     * @return Un string cu adresa clientului
     */
    public String getAddress() {
        return address;
    }

    /**
     * Metoda pentru a seta addresa
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return this.name + ", " + this.address;
    }
}
