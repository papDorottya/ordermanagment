package model;

/**
 * Clasa care echivaleaza un tabel SQL
 * Reprezentand nota de plata cu numele persoanei si totalul de plata
 *
 * @autor Pap Dorottya
 */
public class OrderSum {
    private int id;
    private int clientId;
    private int totalPrice;

    /**
     * Instanteaza un obiect OrderSum cu urmatorii parametrii
     *
     * @param id            the  id
     * @param personId      the person id
     * @param totalPrice    the price
     */
    public OrderSum(int id, int personId, int totalPrice) {
        this.id = id;
        this.clientId = personId;
        this.totalPrice = totalPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Constructor gol. Utilizat pentru crearea obiectului OrderSum de la ...
     */
    public OrderSum() {
    }

    /**
     * Metoda pentru a accesa campul ID din alte metode
     * @return Id-ul persoanei
     */
    public int getClientId() {
        return clientId;
    }

    /**
     * Metoda pentru a seta id-ul persoanei
     * @param clientId
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    /**
     * Metoda pentru a accesa campul totalPrice din alte metode
     * @return pretul total
     */
    public int getTotalPrice() {
        return totalPrice;
    }

    /**
     * Metoda pentru a seta pretul total
     * @param totalPrice
     */
    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
}
