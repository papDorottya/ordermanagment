package model;

/**
 * Clasa care echivaleaza un tabel SQL
 * Reprezentand un produc cu un identificator unic, nume, cantitatea, si pretul
 *
 * @autor Pap Dorottya
 */
public class Product {
    private int id;
    private String name;
    private int quantity;
    private int price;

    /**
     * Instanteaza un obiect Product cu urmatorii parametrii
     *
     * @param id      the id
     * @param name    the name
     * @param quantity the quantity
     * @param price    the price
     */
    public Product(int id, String name, int quantity, int price) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    /**
     * Constructor gol. Utilizat pentru crearea obiectului Produs de la ...
     */
    public Product() {
    }

    /**
     * Metoda pentru a accesa campul ID din alte metode
     * @return Id-ul produsului
     */
    public int getId() {
        return id;
    }

    /**
     * Metoda pentru a seta id-ul
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metoda pentru a accesa campul name din alte metode
     * @return numele produsului
     */
    public String getName() {
        return name;
    }

    /**
     * Metoda pentru a seta numele prodului
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Metoda pentru a accesa campul quantity din alte metode
     * @return cantitatea
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Metoda pentru a seta cantitatea
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Metoda pentru a accesa campul price din alte metode
     * @return pretul
     */
    public int getPrice() {
        return price;
    }

    /**
     * Metoda pentru a seta pretul prodului
     * @param price
     */
    public void setPrice(int price) {
        this.price = price;
    }
}
