package bll;

import bll.validators.ProductValidator;
import bll.validators.Validator;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Product;

import java.util.NoSuchElementException;

public class ProductBLL {
    private ProductDAO productDAO;
    private ProductValidator productValidator;
    private OrderDAO orderDAO;

    public ProductBLL(ProductDAO productDAO, ProductValidator productValidator, OrderDAO orderDAO) {
        this.productDAO = productDAO;
        this.productValidator = productValidator;
        this.orderDAO = orderDAO;
    }

    public ProductBLL() {
        productDAO = new ProductDAO();
        productValidator = new ProductValidator();
        orderDAO = new OrderDAO();

    }

    /**
     * Metoda cauta in baza de date produsul cu id-ul dta ca parametru
     * @param id
     * @return produsul gasit
     * @throws NoSuchElementException
     */
    public Product findById(int id) throws NoSuchElementException {
        Product product = productDAO.findByID(id);
        if(product == null) {
            throw new NoSuchElementException("Not found the product: " + id);
        }
        return product;
    }

    /**
     * Metoda insereaza un produs nou
     * @param id
     * @param name
     * @param quantity
     * @param price
     * @throws Exception
     */
    public void insertNewProduct(int id, String name, int quantity, int price) throws Exception {
        Product p = productDAO.findByName(name);
        if(p == null) {
            Product product = new Product(id, name, quantity, price);
            productValidator.validate(product);
            productDAO.insert(product);
        } else {
            productDAO.update(p.getQuantity() + quantity, "quantity", p.getId());
        }
    }

    /**
     * Metoda care sterge produsul
     * @param name
     */
    public void deleteProduct(String name) {
        Product product = productDAO.findByName(name);
        if(product != null) {
            productDAO.deleteByName(name);
        }
    }
}
