package bll;

import bll.validators.ClientValidator;
import dao.ClientDAO;
import model.Client;

import java.util.NoSuchElementException;

/**
 * Clasa care este responsabila pentru comportamentul logic al obiectului CLient
 */
public class ClientBLL {
    private ClientDAO clientDAO;
    private ClientValidator clientValidator;

    public ClientBLL() {
        clientDAO = new ClientDAO();
        clientValidator = new ClientValidator();
    }


    /**
     * Metoda care gaseste in baza de date clientul cu id-ul dat ca parametru
     * @param id
     * @return clientul daca l-a gasit, daca o exceptie
     * @throws NoSuchElementException
     */
    public Client findById(int id) throws NoSuchElementException {
        Client client = clientDAO.findByID(id);
        if(client == null) {
            throw new NoSuchElementException("Not found client: " + id);
        }
        return client;
    }

    /**
     * Metoda creaza un adauga un nou client in tabel-ul Client in cazul daca nu l-a gasit
     * @param id
     * @param name
     * @param address
     */
    public void insertNewClient(int id, String name, String address) throws Exception{
        if(clientDAO.findByName(name) == null) {
            Client client = new Client(id, name, address);
            clientValidator.validate(client);
            clientDAO.insert(client);
        }
    }

    /**
     * Metoda sterge un client
     * @param name
     * @return returneaza true daca a reusit sa stearga comanda, respectiv false daca nu
     */
    public int deleteClient(String name) {
        return clientDAO.deleteByName(name);
    }
}
