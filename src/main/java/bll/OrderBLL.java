package bll;

import bll.validators.OrderValidator;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.OrderSumDAO;
import dao.ProductDAO;
import model.Client;
import model.Orderr;
import model.OrderSum;
import model.Product;
import presentation.OutputGenerator;

import java.util.NoSuchElementException;

/**
 * Clasa responsabila pentru logica aplicatiei
 * Clasa implementeaza metode speficie asupra clasei(resprectiv tabelului) Order
 */
public class OrderBLL {
    private OrderDAO orderDAO;
    private ClientDAO clientDAO;
    private OrderSumDAO orderSum;
    private ProductDAO productDAO;
    private OrderValidator orderValidator;
    private OrderSumDAO orderSumDAO;
    private OutputGenerator outputGenerator;

    public OrderBLL() {
        orderDAO = new OrderDAO();
        clientDAO = new ClientDAO();
        orderSum = new OrderSumDAO();
        productDAO = new ProductDAO();
        orderValidator = new OrderValidator();
        orderSumDAO = new OrderSumDAO();
        outputGenerator = new OutputGenerator();
    }

    /**
     * Gaseste o comanda dupa id
     * @param id
     * @return id-ul comenzii cautate
     * @throws NoSuchElementException
     */
    public Orderr findById(int id) throws NoSuchElementException {
        Orderr order = orderDAO.findByID(id);
        if(order == null) {
            throw new NoSuchElementException("Order with id = " + id + "not found!");
        }
    return order;
    }

    /**
     * Metoda care creaza o noua comanda
     *
     * Daca nu exista produsul sau clientul(trebuie mai intai sa fie adaugat pe lista de clienti persoanele nou venite)
     * se returneaza o exceptie
     * La fel si daca nu exista suficienta cantitate
     * Altfel in primul rand modific cantitatea de pe stoc
     * Creez o noua comanda in care atribui datele necesare comenzii
     * Apoi se prelucreaza si datele pentru a calcula totalul sumei
     * @param id
     * @param clientName
     * @param product
     * @param quantity
     * @return
     * @throws Exception
     */
    public void insertNewOrder(int id, String clientName, String product, int quantity) throws Exception {
        Client client = clientDAO.findByName(clientName);
        Product product1 = productDAO.findByName(product);
        if (client != null && product1 != null) {
            if (product1.getQuantity() >= quantity) {
                productDAO.update(product1.getQuantity() - quantity, "quantity", product1.getId());
                Orderr order = new Orderr(id, client.getId(), product1.getId(), quantity);
                orderValidator.validate(order);
                orderDAO.insert(order);
                int sumToAddToTotal = order.getQuantity() * product1.getPrice();
                if (orderSumDAO.findByClientId(client.getId()) == null) {
                    OrderSum orderSum = new OrderSum(id, client.getId(), sumToAddToTotal);
                    orderSumDAO.insert(orderSum);
                } else {
                    orderSumDAO.updateByClientId("totalPrice", client.getId(), orderSumDAO.findByClientId(client.getId()).getTotalPrice() + sumToAddToTotal);
                }
                outputGenerator.printOrder(client);
            } else {
                outputGenerator.PrintNotEnoughOnStoc(clientName, product, quantity);
            }
        }
    }

    /**
     * Metoda sterge comanda din baza de date
     * @param id
     * @return returneaza true daca a reusit sa stearga comanda, respectiv false daca nu
     */
    public boolean deleteOrder(int id) {
        return orderDAO.delete(id);
    }

    /**
     * Metoda sterge comanda din baza de date
     * @return returneaza true daca a reusit sa stearga comanda, respectiv false daca nu
     */
    public boolean deleteAll() {
        return orderDAO.deleteAll();
    }
}
