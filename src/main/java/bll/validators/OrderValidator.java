package bll.validators;

import model.Orderr;

public class OrderValidator implements Validator<Orderr> {
    @Override
    public void validate(Orderr order) throws Exception {
        if(order.getQuantity() <= 0) {
            throw new Exception("Wrong order quantity");
        }
    }
}
