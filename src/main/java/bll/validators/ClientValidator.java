package bll.validators;

import model.Client;

/**
 * Clasa pentru a valida numele clientului
 * Nu se valideaza si orasul, se considera ca clientul introduce un oras corect
 *
 */
public class ClientValidator implements Validator<Client>{
    /**
     * Metoda valideaza numele clientului, se verifica daca numele este compus doar din litere
     * Desigur nu am luat in calcul si numele fiului, lui Elon Musk :)))
     * @param client
     * @return true daca e corect numele clientului
     * @throws Exception
     */
    @Override
    public void validate(Client client) throws Exception {
        if(!client.getName().isEmpty() && !client.getName().matches("^[a-zA-Z\\s]*$")) {
            throw new Exception("Wrong name");
        }
    }
}
