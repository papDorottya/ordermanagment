package bll.validators;

import model.Product;

public class ProductValidator implements Validator<Product>{

    @Override
    public void validate(Product product) throws Exception {
        if(!product.getName().isEmpty() && !product.getName().matches("^[a-zA-Z\\s0-9]*$")) {
            throw new Exception("Wrong product name");
        }
        if(product.getPrice() <= 0) {
            throw new Exception("Wrong product price");
        }
        if(product.getQuantity() <= 0) {
            throw  new Exception("Wrong product quantity");
        }
    }
}
