package connection;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clasa pentru a conecta baza de date de java
 *
 */
public class ConnectionFactory {

    /**
     * Clasa contine numele driver-ului, locatia, user-ul si parola pentru a accesa MySQL Server-ul
     */
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://127.0.0.1:3306/ordemanagement?useSSL=false";
    private static final String USER = "root";
    private static final String PASS = "Betonarmat5";

    private static ConnectionFactory singleInstance = new ConnectionFactory();

    /**
     * Conexiunea este plasata intr-un obiect Singleton
     */
    public ConnectionFactory() {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Urmatoarele metode creaza conexiunea, o activeaza, apoi inchide conexiunea, Statement-ul si ResultSet-ul
     */
    private Connection createConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DBURL, USER, PASS);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
            e.printStackTrace();
        }

        return conn;
    }

    public static Connection getConnection() {
        return singleInstance.createConnection();
    }

    public static void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
        }
    }

    public static void close(Statement statement) {
        try {
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
        }
    }

    public static void close(ResultSet resultSet) {
        try {
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, "An error occured while trying to close the ResultSet");
        }
    }
}